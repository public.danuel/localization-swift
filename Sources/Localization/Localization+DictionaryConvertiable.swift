protocol DictionaryConvertible {
  func toDictionary () -> [String: CustomStringConvertible]
}
