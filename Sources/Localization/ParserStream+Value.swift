internal enum Value {
  case String(value: String)
  case Int(value: Int)
  case Bool(value: Bool)
  case Null
}
