internal typealias Attributes = [String: Value]

internal enum Node {
  case BlockTag(name: String, attributes: Attributes, children: [Node] = [])
  case InlineTag(name: String, attributes: Attributes)
  case Reference(name: String)
  case Variable(name: String)
  case Text(value: String)
  case Linebreak(count: Int)
  case Whitespace(count: Int)
}
