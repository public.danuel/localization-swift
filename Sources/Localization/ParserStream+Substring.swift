internal extension Substring {
  var isNumeric: Bool {
    get {
      return ("0"..."9").contains(self)
    }
  }
}
