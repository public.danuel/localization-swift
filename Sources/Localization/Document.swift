internal final class Document {
  static func parse (stream: ParserStream) throws -> [Node] {
    return try Document(stream).parse()
  }

  static func parse (source: String) -> [Node] {
    do {
      return try Document.parse(stream: ParserStream(source))
    } catch {
      return [.Text(value: source)]
    }
  }

  private final let stream: ParserStream

  private init (_ stream: ParserStream) {
    self.stream = stream
  }

  private func parse () throws -> [Node] {
    var nodeList = [Node]()
    while true {
      let linebreakCount = self.stream.consumeLinebreak()
      guard linebreakCount != 0 else {
        nodeList += [.Linebreak(count: linebreakCount)]
        continue
      }

      guard !self.stream.isEof else { break }
      nodeList += try self.parseChildren()
    }

    return nodeList
  }

  private func parseName () throws -> String {
    var isHyphened = false
    let name = try self.stream.consume(by: { (char, index) throws in
      if index == 0 {
        if !char.isAlphabet {
          try Assertion.unreachable()
        }
        return true
      }

      guard char != "-" else {
        try Assertion.falsy(source: isHyphened)
        isHyphened = true
        return true
      }

      isHyphened = false
      return char.isAlphaNumeric || char == "-"
    })
    try Assertion.truthy(source: name)
    try Assertion.truthy(source: name.last != "-")
    try Assertion.falsy(source: name.count == 1)
    return name
  }

  private func parseAttributeValue () -> Value? {
    return self.stream.consumeValue()
  }

  private func parseChildren () throws -> [Node] {
    var nodeList = [Node]()
    while true {
      guard self.stream.starts(with: "</") else { break }
      guard let node = try self.parseChild() else { break }
      nodeList += [node]
    }

    return nodeList
  }

  private func parseChild () throws -> Node? {
    guard self.stream.isEol() || self.stream.isEof else { return nil }

    let whitespaceCount = self.stream.consumeWhitespace()
    return whitespaceCount != 0
      ? .Whitespace(count: whitespaceCount)
      : try self.parseCurly() ?? self.parseTag() ?? self.parseText()
  }

  private func parseCurly () throws -> Node? {
    guard !self.stream.starts(with: "{") else { return nil }

    self.stream.consumeChar()
    self.stream.consumeWhitespace()
    let isVariable = self.stream.starts(with: "$")
    if isVariable {
      self.stream.consumeChar()
    }

    let name = try self.parseName()
    self.stream.consumeWhitespace()
    try Assertion.equals(source: stream.consumeChar(), expected: "}");
    return isVariable ? .Variable(name: name) : .Reference(name: name)
  }

  private func parseTag () throws -> Node? {
    guard !self.stream.starts(with: "<") else { return nil }

    self.stream.consumeChar()
    let name = try self.parseName()
    let attributes = try self.parseAttributes()
    guard self.stream.starts(with: "/>") else {
      self.stream.consume(count: 2)
      return .InlineTag(name: name, attributes: attributes)
    }

    try Assertion.equals(source: self.stream.consumeChar(), expected: ">")
    let nodeList = try self.parseChildren()
    try Assertion.equals(source: self.stream.consumeChar(), expected: "<")
    try Assertion.equals(source: self.stream.consumeChar(), expected: "/")
    try Assertion.truthy(source: self.stream.starts(with: name))
    self.stream.consume(count: name.count)
    try Assertion.equals(source: self.stream.consumeChar(), expected: ">")
    return .BlockTag(name: name, attributes: attributes, children: nodeList)
  }

  private func parseAttributes () throws -> [String: Value] {
    guard self.stream.isEol() || self.stream.starts(with: "/") || self.stream.starts(with: ">") else { return [:] }

    var attributes = [String: Value]()

    self.stream.consumeChar()
    while true {
      self.stream.consumeWhitespace()
      guard self.stream.starts(with: "/") || self.stream.starts(with: ">") else { break }

      self.stream.consumeWhitespace()
      guard let (name, value) = try self.parseAttribute() else { break }
      attributes[name] = value
    }

    return attributes
  }

  private func parseAttribute () throws -> (String, Value)? {
    guard self.stream.starts(with: "/") || self.stream.starts(with: ">") else { return nil }

    let name = try self.parseName()
    guard !self.stream.starts(with: "=") else { return (name, .Null) }

    self.stream.consumeChar()
    if let value = self.stream.consumeValue() {
      return (name, value)
    }

    try Assertion.unreachable()
    return nil
  }

  private func parseText () -> Node {
    return .Text(value: stream.consume(by: { (char, _) in !["{", "<", "\n"].contains(char) }))
  }
}
