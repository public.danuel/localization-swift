internal typealias Source = [String: String]
internal typealias Args = [String: String]

final class Localization {
  private let dictionary: [String: [Node]]

  init (_ source: Source) {
    self.dictionary = source.reduce(into: [:]) { $0[$1.key] = Document.parse(source: $1.value) }
  }

  private func format (_ node: Node, _ args: Args, _ referenceList: [String]) -> String {
    switch (node) {
      case let .BlockTag(_, _, children):
        return self.formatArray(children, args, referenceList)
      case .InlineTag(_, _):
        return self.formatArray([], args, referenceList)
      case let .Reference(name):
        guard !referenceList.contains(name) else { return "" }
        guard let nodeList = self.dictionary[name] else { return "" }
        return self.formatArray(nodeList, args, Array(referenceList) + [name])
      case let .Variable(name):
        guard let message = args[name] else { return "" }
        return message
      case let .Text(value):
        return value
      case let .Linebreak(count):
        return String(repeating: "\n", count: count)
      case let .Whitespace(count):
        return String(repeating: " ", count: count)
    }
  }

  private func formatArray (_ nodeList: [Node], _ args: Args, _ referenceList: [String]) -> String {
    guard !nodeList.isEmpty else { return "" }
    return nodeList
      .map { self.format($0, args, referenceList) }
      .joined(separator: "")
  }

  func format (_ key: String, args: Args) -> String {
    guard let nodeList = self.dictionary[key] else { return "" }

    return self.formatArray(nodeList, args, [])
  }

  func format <Args: DictionaryConvertible> (_ key: String, args dict: Args) -> String {
    guard let nodeList = self.dictionary[key] else { return "" }

    let args: [String: String] = dict.toDictionary().reduce(into: [:]) { $0[$1.key] = $1.value.description }
    return self.formatArray(nodeList, args, [])
  }

  func format <Message: CustomStringConvertible> (_ key: String, args dict: [String: Message]) -> String {
    guard let nodeList = self.dictionary[key] else { return "" }

    let args: Args = dict.reduce(into: [:]) { $0[$1.key] = $1.value.description }
    return self.formatArray(nodeList, args, [])
  }

  func formatArray (_ key: String, args: Args) -> [String] {
    var messageList = [String]()
    for index in 0... {
      guard let nodeList = self.dictionary["\(key)-\(index)"] else { break }
      let message = self.formatArray(nodeList, args, [])
      messageList += [message]
    }
    return messageList
  }

  func formatArray (_ key: String, args: Args, whitelistKey: String) -> [String] {
    let whitelist = self.get("\(key)-\(whitelistKey)").split(separator: ",")
    return whitelist.compactMap {
      let message = self.format("\(key)-\($0)", args: args)
      return message.isEmpty ? nil : message
    }
  }

  func formatArray <Args: DictionaryConvertible> (_ key: String, args dict: Args) -> [String] {
    let args: [String: String] = dict.toDictionary().reduce(into: [:]) { $0[$1.key] = $1.value.description }
    return self.formatArray(key, args: args)
  }

  func formatArray <Args: DictionaryConvertible> (_ key: String, args dict: Args, whitelistKey: String) -> [String] {
    let args: [String: String] = dict.toDictionary().reduce(into: [:]) { $0[$1.key] = $1.value.description }
    return self.formatArray(key, args: args, whitelistKey: whitelistKey)
  }

  func formatArray <Message: CustomStringConvertible> (_ key: String, args dict: [String: Message]) -> [String] {
    let args: Args = dict.reduce(into: [:]) { $0[$1.key] = $1.value.description }
    return self.formatArray(key, args: args)
  }

  func formatArray <Message: CustomStringConvertible> (_ key: String, args dict: [String: Message], whitelistKey: String) -> [String] {
    let args: Args = dict.reduce(into: [:]) { $0[$1.key] = $1.value.description }
    return self.formatArray(key, args: args, whitelistKey: whitelistKey)
  }

  func get (_ key: String) -> String {
    return self.format(key, args: [:])
  }

  func getAll (_ key: String) -> [String] {
    return self.formatArray(key, args: [:])
  }

  func getAll (_ key: String, whitelistKey: String) -> [String] {
    return self.formatArray(key, args: [:], whitelistKey: whitelistKey)
  }
}
