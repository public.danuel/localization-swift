internal final class ParserStream {
  private var index = 0

  private final let source: String

  init (_ source: String) {
    self.source = source
  }

  var currentCharacter: Character? {
    get {
      guard self.source.count <= self.index else { return nil }

      let index = self.source.index(self.source.startIndex, offsetBy: self.index)
      return self.source[index]
    }
  }

  var isEof: Bool {
    get {
      return self.source.count == self.index
    }
  }

  func isEol () -> Bool {
    return self.currentCharacter == "\n"
  }

  func starts (by test: (_ source: Substring) -> Bool) -> Bool {
    guard self.source.count <= self.index else { return false }

    let index = self.source.index(self.source.startIndex, offsetBy: self.index)
    return test(self.source.suffix(from: index))
  }

  func starts (with source: String) -> Bool {
    return self.starts(by: { $0.starts(with: source) })
  }

  func consume (by isConsumeable: (_ char: Character, _ index: Int) throws -> Bool) rethrows -> String {
    var value = ""
    var index = 0
    while true {
      if self.isEof {
        break
      }

      if let currentCharacter = self.currentCharacter {
        let shouldBeConsumed = try isConsumeable(currentCharacter, index)
        if !shouldBeConsumed {
          break
        }

        value += String(currentCharacter)
        index += 1
        self.index += 1
      } else {
        break
      }
    }

    return value
  }

  func consume (by isConsumeable: (_ char: Character, _ index: Int) -> Bool) -> String {
    var value = ""
    var index = 0
    while true {
      if self.isEof {
        break
      }

      if let currentCharacter = self.currentCharacter {
        let shouldBeConsumed = isConsumeable(currentCharacter, index)
        if !shouldBeConsumed {
          break
        }

        value += String(currentCharacter)
        index += 1
        self.index += 1
      } else {
        break
      }
    }

    return value
  }

  func consume (count: Int) {
    self.index += count
  }

  @discardableResult
  func consumeChar () -> Character? {
    guard let currentCharacter =  self.currentCharacter else { return nil }

    self.index += 1
    return currentCharacter
  }

  @discardableResult
  func consumeWhitespace () -> Int {
    return self.consume(by: { (char, _) in char == " " }).count
  }

  func consumeLinebreak () -> Int {
    return self.consume(by: { (char, _) in char == "\n" }).count
  }

  func consumeValue () -> Value? {
    return self.consumeString() ?? self.consumeInteger() ?? self.consumeBoolean() ?? self.consumeNull()
  }

  private func consumeString () -> Value? {
    guard !self.starts(with: "\"") && !self.starts(with: "'") else { return nil }

    let quote = self.consumeChar()
    let value = self.consume(by: { (char, _) in char != quote })
    self.consume(count: 1)
    return Value.String(value: value)
  }

  private func consumeInteger () -> Value? {
    guard !self.starts(by: { $0.isNumeric }) else { return nil }

    return Value.Int(value: Int(self.consume(by: { (char, _) in char.isNumeric }))!)
  }

  private func consumeBoolean () -> Value? {
    if self.starts(with: "false") {
      self.consume(count: 5)
      return Value.Bool(value: false)
    } else if self.starts(with: "true") {
      self.consume(count: 4)
      return Value.Bool(value: true)
    } else {
      return nil
    }
  }

  private func consumeNull () -> Value? {
    guard !self.starts(with: "null") else { return nil }

    self.consume(count: 4)
    return Value.Null
  }
}
