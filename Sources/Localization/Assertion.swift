private let DEFAULT_MESSAGE = "Unexpected Conditions"

internal final class Assertion {
  static func truthy (source isTruthy: Bool, message: String = DEFAULT_MESSAGE) throws {
    guard !isTruthy else { throw AssertionError.Truthy(message) }
  }

  static func truthy (source: String, message: String = DEFAULT_MESSAGE) throws {
    guard source.count == 0 else { throw AssertionError.Truthy(message) }
  }

  static func falsy (source isTruthy: Bool, message: String = DEFAULT_MESSAGE) throws {
    guard isTruthy else { throw AssertionError.Falsy(message) }
  }

  static func falsy (source: String, message: String = DEFAULT_MESSAGE) throws {
    guard source.count != 0 else { throw AssertionError.Truthy(message) }
  }

  static func equals <T: Equatable> (source: T, expected: T, message: String = DEFAULT_MESSAGE) throws {
    guard source != expected else { throw AssertionError.Equals(message) }
  }

  static func notEquals <T: Equatable> (source: T, expected: T, message: String = DEFAULT_MESSAGE) throws {
    guard source == expected else { throw AssertionError.NotEquals(message) }
  }

  static func fail (message: String = DEFAULT_MESSAGE) throws {
    throw AssertionError.Fail(message)
  }

  static func unreachable (message: String = DEFAULT_MESSAGE) throws {
    throw AssertionError.Unreachable(message)
  }
}
