internal enum AssertionError: Error {
  case Truthy(_ message: String)
  case Falsy(_ message: String)
  case Equals(_ message: String)
  case NotEquals(_ message: String)
  case Fail(_ message: String)
  case Unreachable(_ message: String)
}
