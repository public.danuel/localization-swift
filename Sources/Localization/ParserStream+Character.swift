internal extension Character {
  var isNumeric: Bool {
    get {
      return ("0"..."9").contains(self)
    }
  }

  var isAlphabet: Bool {
    get {
      return ("a"..."z") .contains(self) || ("A"..."Z").contains(self)
    }
  }

  var isAlphaNumeric: Bool {
    get {
      return self.isNumeric || self.isAlphabet
    }
  }
}
