import XCTest
@testable import Localization

final class LocalizationTests: XCTestCase {
  func getEmptyMessageInEmptyDictionaryWithKey() {
    let source: Args = [:]
    let localization = Localization(source)
    XCTAssertEqual(localization.get("foo"), "")
    XCTAssertEqual(localization.getAll("foo"), [])
    XCTAssertEqual(localization.format("foo", args: [:]), "")
    XCTAssertEqual(localization.formatArray("foo", args: [:]), [])
    XCTAssertEqual(localization.get("foo-bar"), "")
    XCTAssertEqual(localization.getAll("foo-bar"), [])
    XCTAssertEqual(localization.format("foo-bar", args: [:]), "")
    XCTAssertEqual(localization.formatArray("foo-bar", args: [:]), [])
  }

  func getMessageInDictionaryWithKey () {
    let source = [
      "foo": "hello world",
      "foo-0": "hello world",
      "foo-bar": "hello world",
      "foo-bar-0": "hello world"
    ]
    let localization = Localization(source)
    XCTAssertEqual(localization.get("foo"), "hello world")
    XCTAssertEqual(localization.getAll("foo"), ["hello world"])
    XCTAssertEqual(localization.format("foo", args: [:]), "hello world")
    XCTAssertEqual(localization.formatArray("foo", args: [:]), ["hello world"])
    XCTAssertEqual(localization.get("foo-bar"), "hello world")
    XCTAssertEqual(localization.getAll("foo-bar"), ["hello world"])
    XCTAssertEqual(localization.format("foo-bar", args: [:]), "hello world")
    XCTAssertEqual(localization.formatArray("foo-bar", args: [:]), ["hello world"])
  }

  func getReferencedMessageInRecordWithKey () {
    let source = [
      "foo": "hello {bar}",
      "bar": "world",
      "foo-0": "hello {bar}",
      "foo-bar": "hello {bar}",
      "foo-bar-0": "hello {bar}"
    ]
    let localization = Localization(source)
    XCTAssertEqual(localization.get("foo"), "hello world")
    XCTAssertEqual(localization.getAll("foo"), ["hello world"])
    XCTAssertEqual(localization.format("foo", args: [:]), "hello world")
    XCTAssertEqual(localization.formatArray("foo", args: [:]), ["hello world"])
    XCTAssertEqual(localization.get("foo-bar"), "hello world")
    XCTAssertEqual(localization.getAll("foo-bar"), ["hello world"])
    XCTAssertEqual(localization.format("foo-bar", args: [:]), "hello world")
    XCTAssertEqual(localization.formatArray("foo-bar", args: [:]), ["hello world"])
  }

  func getFormattedMessageInRecordWithKey () {
    let source = [
      "foo": "hello {$bar}",
      "foo-0": "hello {$bar}",
      "foo-bar": "hello {$bar}",
      "foo-bar-0": "hello {$bar}"
    ]
    let localization = Localization(source)
    XCTAssertEqual(localization.get("foo"), "hello ")
    XCTAssertEqual(localization.getAll("foo"), ["hello "])
    XCTAssertEqual(localization.format("foo", args: [ "bar": "world" ]), "hello world")
    XCTAssertEqual(localization.formatArray("foo", args: [ "bar": "world" ]), ["hello world"])
    XCTAssertEqual(localization.format("foo", args: [ "bar": 123 ]), "hello 123")
    XCTAssertEqual(localization.formatArray("foo", args: [ "bar": 123 ]), ["hello 123"])
  }
    
  static var allTests = [
    ("getEmptyMessageInEmptyDictionaryWithKey", getEmptyMessageInEmptyDictionaryWithKey),
    ("getMessageInDictionaryWithKey", getMessageInDictionaryWithKey),
    ("getReferencedMessageInRecordWithKey", getReferencedMessageInRecordWithKey),
    ("getFormattedMessageInRecordWithKey", getFormattedMessageInRecordWithKey)
  ]
}
